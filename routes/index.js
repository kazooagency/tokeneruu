var authTools = require("../auth/authTools");
var jwt = require("jsonwebtoken");
var express = require("express");
var router = express.Router();

var mysql = require("mysql");
var connection = mysql.createConnection({
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
  port: process.env.MYSQL_PORT
});

router.get("/", (req, res) => res.send("Hello World!"));

router.post("/register", (req, res) => {
  res.send("Register ok");

  connection.query(
    "INSERT INTO user (email, hashedpassword) VALUES ('" +
      req.body.email +
      "', '" +
      req.body.password +
      "');",
    function(error) {
      if (error) {
        console.log(error);
      }
    }
  );
});

router.post("/login", (req, res) => {
  connection.query(
    "select * from user where email='" + req.body.email + "';",
    function(error, results) {
      if (error) {
        res.status(401).send({ auth: false, message: "Login not OK" });
      } else {
        if (results[0]) {
          if (results[0].hashedpassword === req.body.password) {
            var token = authTools.generateToken(req.body.email);
            res
              .status(200)
              .send({ auth: true, message: "Login OK", token: token });
          } else {
            res.status(401).send({ auth: false, message: "Login not OK" });
          }
        } else {
          res.status(401).send({ auth: false, message: "Login not OK" });
        }
      }
    }
  );
});

router.get("/protected", authTools.verifyToken, (req, res) => {
  res.send("Protected ok");
});

module.exports = router;
