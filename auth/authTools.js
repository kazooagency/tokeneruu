var jwt = require("jsonwebtoken");

module.exports = {
  generateToken: function(email) {
    return jwt.sign({ email: email }, process.env.JWT_SECRET);
  },
  verifyToken: function(req, res, next) {
    console.log("verify token");
    var token = req.headers.authorization;
    console.log("token: " + token);
    if (!token) {
      console.log("token: " + token);
      return res
        .status(401)
        .send({ auth: false, message: "No token provided." });
    }

    jwt.verify(token.replace("Bearer ", ""), process.env.JWT_SECRET, function(
      err,
      decoded
    ) {
      if (err) {
        return res
          .status(500)
          .send({ auth: false, message: "Failed to authenticate token." });
      }

      return next();
    });
  }
};
